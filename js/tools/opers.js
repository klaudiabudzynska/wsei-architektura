const mov = (outputs, source, dest) => {
    outputs[dest].value = outputs[source].value;

    createNotification(NOTIFICATION_TYPES.mov);
}
const xchg = (outputs, source, dest) => {
    const buffer = outputs[dest].value;
    outputs[dest].value = outputs[source].value;
    outputs[source].value = buffer;

    createNotification(NOTIFICATION_TYPES.xchg)
}

const procMemMov = (outputs, memoryAddress, generalUseRegister, direction) => {
    if (direction) {
        switch (direction) {
            case '0':
                memory[memoryAddress] = outputs[generalUseRegister].value.slice(2, 4);
                memory[memoryAddress + 1] = outputs[generalUseRegister].value.slice(0, 2);

                createNotification(NOTIFICATION_TYPES.movProcMem);
                break;
            case '1':
                outputs[generalUseRegister].value = memory[memoryAddress + 1] + memory[memoryAddress];
                createNotification(NOTIFICATION_TYPES.movMemProc);
                break;
        }
    } else {
        createNotification(NOTIFICATION_TYPES.movProcMemDirectionError);
    }
}

const procMemXchg = (outputs, memoryAddress, generalUseRegister) => {
    const buffer = memory[memoryAddress + 1] + memory[memoryAddress];

    memory[memoryAddress] = outputs[generalUseRegister].value.slice(2, 4);
    memory[memoryAddress + 1] = outputs[generalUseRegister].value.slice(0, 2);

    outputs[generalUseRegister].value = buffer;

    createNotification(NOTIFICATION_TYPES.xchg);
}

const push = (sp, source) => {
    if (sp && source) {
        const stackAddress = memoryAddressParse(sp)

        stack[stackAddress] = source.slice(2, 4);
        stack[stackAddress + 1] = source.slice(0, 2);

        sp.value = fillBlank(
            memoryAddressParse(sp, {value: '0002'})
                .toString(16)
        );

        createNotification(NOTIFICATION_TYPES.push);
    } else {
        createNotification(NOTIFICATION_TYPES.pushError);
    }
}

const pop = (sp, register) => {
    if (sp && memoryAddressParse(sp) >= 2 &&
        stack[memoryAddressParse(sp) - 1] &&
        register
    ) {
        sp.value = fillBlank(
            memoryAddressParse(sp, {value: '0002'}, -1)
                .toString(16)
        );

        const stackAddress = memoryAddressParse(sp)

        register.value = stack[stackAddress + 1] + stack[stackAddress];
        stack[stackAddress] = stack[stackAddress + 1] = undefined;

        createNotification(NOTIFICATION_TYPES.pop);
    } else {
        createNotification(NOTIFICATION_TYPES.popError);
    }
}