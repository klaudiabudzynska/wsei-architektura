const randomHexValue = () => {
    const HEX_DIGITS = 16 * 16 * 16 * 16

    return fillBlank(
        Math.floor(Math.random() * HEX_DIGITS).toString(16)
    );
}

const memoryAddressParse = (first, second, modifier) => {
    if (modifier) {
        second.value = (parseInt(second.value, 16) * modifier).toString(16);
    }

    return second ?
        parseInt(first.value, 16) + parseInt(second.value, 16)
        :
        parseInt(first.value, 16);
}

const fillBlank = (value) => {
    while (value.length < 4) {
        value = '0' + value;
    }

    return value
}