let memory = [];
let stack = [];

for (let i = 0; i < 64 * 1024; ++i) {
    memory.push('00');
}

const OPERATION_TYPE = {
    MOV: 'mov',
    XCHG: 'xchg',
    PUSH: 'push',
    POP: 'pop'
}