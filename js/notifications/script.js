const NOTIFICATION_TYPES = {
    addValueToRegister: {
        description: 'dodano wartości do rejestrów',
        type: 'success'
    },
    mov: {
        description: 'operacja mov zakończona',
        type: 'success'
    },
    xchg: {
        description: 'operacja xchg zakończona',
        type: 'success'
    },
    movXchgError: {
        description: 'niepowodzenie - zaznacz źródło i cel operacji',
        type: 'error'
    },
    movProcMem: {
        description: 'przesłano wartość z rejestru do pamięci',
        type: 'success'
    },
    movMemProc: {
        description: 'przesłano wartość z pamięci do rejestru',
        type: 'success'
    },
    movProcMemDirectionError: {
        description: 'niepowodzenie - nie podano kierunku operacji',
        type: 'error'
    },
    push: {
        description: 'operacja push zakończona',
        type: 'success'
    },
    pushError: {
        description: 'nie można wykonać operacji',
        type: 'error'
    },
    pop: {
        description: 'operacja pop zakończona',
        type: 'success'
    },
    popError: {
        description: 'nie można wykonać operacji',
        type: 'error'
    }
}

const getNotificationsContainer = () => {
    return document.getElementById('notification-container');
}

const getRemoveAllButton = () => {
    return document.getElementById('remove-all');
}

const allowAllNotificationsRemove = () => {
    getRemoveAllButton().addEventListener('click', () => {
        const notifications = document.getElementsByClassName('notification');

        getRemoveAllButton().classList.remove('displayed');

        for (let i = 0; i < notifications.length; ++i) {
            closeAnimation(notifications[i]);
        }
    })
}

const createNotification = ({description, type}) => {
    const container = getNotificationsContainer();

    const text = document.createElement('p');
    const notificationElement = document.createElement('div')
    const close = document.createElement('i');

    notificationElement.classList.add('notification', `notification--${type}`);
    text.classList.add('notification__description');
    close.classList.add('fas', 'fa-times', 'notification__close');

    text.innerHTML = description;

    close.addEventListener('click', (e) => {
        closeAnimation(e.target.parentElement);
    })

    showRemoveAllButton()

    setTimeout(() => {
        closeAnimation(notificationElement);
    }, 3500);

    notificationElement.appendChild(text);
    notificationElement.appendChild(close);

    container.insertBefore(
        notificationElement,
        container.firstChild
    );
}

const closeAnimation = (element) => {
    element.classList.add('notification--closing');
    setTimeout(() => {
        element.remove();
        showRemoveAllButton();
    }, 800);
}

const showRemoveAllButton = () => {
    if (getNotificationsContainer().childElementCount > 2) {
        getRemoveAllButton().classList.add('displayed');
    } else {
        getRemoveAllButton().classList.remove('displayed');
    }
}