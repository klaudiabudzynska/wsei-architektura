window.addEventListener('load', () => {
    const resetRegistersButton = document.getElementById('register-reset-values');
    const randomRegistersButton = document.getElementById('register-random-values');

    const saveRegisterValuesButton = document.getElementById('register-submit');

    const movRegistersButton = document.getElementById('mov-submit');
    const xchgRegistersButton = document.getElementById('xchg-submit');

    const movProcMemButton = document.getElementById('proc-mem-mov');
    const xchgProcMemButton = document.getElementById('proc-mem-xchg');

    const pushStackButton = document.getElementById('stack-push');
    const popStackButton = document.getElementById('stack-pop');

    const registersInput = {
        ax: document.getElementById('register-input-ax'),
        bx: document.getElementById('register-input-bx'),
        cx: document.getElementById('register-input-cx'),
        dx: document.getElementById('register-input-dx'),
        si: document.getElementById('register-input-si'),
        di: document.getElementById('register-input-di'),
        bp: document.getElementById('register-input-bp'),
        sp: document.getElementById('register-input-sp'),
        disp: document.getElementById('register-input-disp'),
    }
    const registersOutput = {
        ax: document.getElementById('register-output-ax'),
        bx: document.getElementById('register-output-bx'),
        cx: document.getElementById('register-output-cx'),
        dx: document.getElementById('register-output-dx'),
        si: document.getElementById('register-output-si'),
        di: document.getElementById('register-output-di'),
        bp: document.getElementById('register-output-bp'),
        sp: document.getElementById('register-output-sp'),
        disp: document.getElementById('register-output-disp'),
    }

    const generalUseRegisters = ['ax', 'bx', 'cx', 'dx'];

    allowAllNotificationsRemove();

    saveRegisterValuesButton.addEventListener('click', (e) => {
        e.preventDefault();
        saveRegisterValues(registersInput, registersOutput);
    });

    resetRegistersButton.addEventListener('click', (e) => {
        e.preventDefault();
        resetRegisterValues(registersOutput);
    });

    randomRegistersButton.addEventListener('click', (e) => {
        e.preventDefault();
        randomRegisterValues(registersOutput, generalUseRegisters);
    });

    movRegistersButton.addEventListener('click', (e) => {
        e.preventDefault();
        movXchgRegisters(registersOutput, OPERATION_TYPE.MOV);
    });

    xchgRegistersButton.addEventListener('click', (e) => {
        e.preventDefault();
        movXchgRegisters(registersOutput, OPERATION_TYPE.XCHG);
    });

    movProcMemButton.addEventListener('click', (e) => {
        e.preventDefault();
        movXchgProcMem(registersOutput, OPERATION_TYPE.MOV);
    });

    xchgProcMemButton.addEventListener('click', (e) => {
        e.preventDefault();
        movXchgProcMem(registersOutput, OPERATION_TYPE.XCHG);
    });

    const procMemModeRadios = document.getElementsByName("proc-mem-mode");
    for(let i = 0; i < procMemModeRadios.length; i++) {
        procMemModeRadios[i].onclick = function() {
            switchMode(this.value);
        }
    }

    pushStackButton.addEventListener('click', (e) => {
        e.preventDefault();
        pushPopStack(registersOutput, OPERATION_TYPE.PUSH);
    })

    popStackButton.addEventListener('click', (e) => {
        e.preventDefault();
        pushPopStack(registersOutput, OPERATION_TYPE.POP);
    })
})

const saveRegisterValues = (inputs, outputs) => {
    for (let input in inputs) {
        if (inputs.hasOwnProperty(input) &&
            outputs.hasOwnProperty(input) &&
            inputs[input].value
        ) {
            if (isRegisterInputValid(inputs[input].value)) {
                outputs[input].value = inputs[input].value;
                inputs[input].classList.remove('error');
            } else {
                inputs[input].classList.add('error');
            }
            inputs[input].value = null;
        }
    }
    createNotification(NOTIFICATION_TYPES.addValueToRegister);
}

const resetRegisterValues = (outputs) => {
    for (let output in outputs) {
        if (outputs.hasOwnProperty(output)) {
            outputs[output].value = '0000';
        }
    }
}

const randomRegisterValues = (outputs, registers) => {
    registers.forEach((register) => {
        if (outputs.hasOwnProperty(register)) {
            outputs[register].value = randomHexValue();
        }
    })
    createNotification(NOTIFICATION_TYPES.addValueToRegister);
}

const movXchgRegisters = (outputs, operationType) => {
    const source = document.querySelector('input[name="mov-xchg-source"]:checked');
    const dest = document.querySelector('input[name="mov-xchg-dest"]:checked');

    if (source?.value &&
        dest?.value &&
        outputs.hasOwnProperty(source.value) &&
        outputs.hasOwnProperty(dest.value)
    ) {
        const attributes = [outputs, source.value, dest.value]
        operationType === OPERATION_TYPE.MOV ?
        mov(...attributes)
        :
        xchg(...attributes)
    } else {
        createNotification(NOTIFICATION_TYPES.movXchgError);
    }
}

const switchMode = (enabledMode) => {
    ['indexed', 'base', 'indexed-base'].forEach((mode, index) => {
        const procMemModeGroup = document.getElementsByName(`proc-mem-${mode}`);
        if (index === parseInt(enabledMode)) {
            for(let i = 0; i < procMemModeGroup.length; i++) {
                procMemModeGroup[i].removeAttribute('disabled')
            }
        } else {
            for(let i = 0; i < procMemModeGroup.length; i++) {
                procMemModeGroup[i].setAttribute('disabled', 'true')
                procMemModeGroup[i].checked = false
            }
        }
    })
}

const movXchgProcMem = (outputs, operationType) => {
    const direction = document.querySelector('input[name="proc-mem-dir"]:checked');
    const generalUseRegister = document.querySelector('input[name="proc-mem-source"]:checked');
    const mode = document.querySelector('input[name="proc-mem-mode"]:checked');
    const indexedMode = document.querySelector('input[name="proc-mem-indexed"]:checked');
    const baseMode = document.querySelector('input[name="proc-mem-base"]:checked');
    const indexedBaseMode = document.querySelector('input[name="proc-mem-indexed-base"]:checked');

    if (generalUseRegister?.value &&
        mode?.value &&
        (indexedMode?.value || baseMode?.value || indexedBaseMode?.value)
    ) {
        const radioRegisterChoice = indexedBaseMode?.value || baseMode?.value || indexedBaseMode?.value;
        let memoryAddressFromRegister = '0000';

        switch (mode.value) {
            case '0':
                memoryAddressFromRegister = radioRegisterChoice
                    ? memoryAddressParse(outputs.di)
                    : memoryAddressParse(outputs.si)
                break;
            case '1':
                memoryAddressFromRegister = radioRegisterChoice
                    ? memoryAddressParse(outputs.bp)
                    : memoryAddressParse(outputs.bx)
                break;
            case '2':
                switch (radioRegisterChoice) {
                    case '0':
                        memoryAddressFromRegister = memoryAddressParse(outputs.si, outputs.bx);
                        break;
                    case '1':
                        memoryAddressFromRegister = memoryAddressParse(outputs.di, outputs.bx)
                        break;
                    case '2':
                        memoryAddressFromRegister = memoryAddressParse(outputs.si, outputs.bp);
                        break;
                    case '3':
                        memoryAddressFromRegister = memoryAddressParse(outputs.di, outputs.bp);
                        break;
                }
        }

        const attributes = [
            outputs,
            memoryAddressFromRegister + memoryAddressParse(outputs.disp),
            generalUseRegister.value,
            direction?.value
        ];

        operationType === OPERATION_TYPE.MOV ?
        procMemMov(...attributes)
        :
        procMemXchg(...attributes);
    }
    else {
        createNotification(NOTIFICATION_TYPES.movXchgError);
    }
}

const pushPopStack = (outputs, operationType) => {
    const sourceRegister = document.querySelector('input[name="stack-source"]:checked');

    operationType === OPERATION_TYPE.PUSH ?
        push(outputs.sp, outputs[sourceRegister.value].value)
        :
        pop(outputs.sp, outputs[sourceRegister.value]);
}

